import React from 'react';
import ReactDOM from 'react-dom';

function MiPrueba() {
    return (
        <div className="container mx-auto">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Mi prueba</div>

                        <div className="card-body">Esta es mi prueba</div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default MiPrueba;

if (document.getElementById('mi-prueba')) {
    ReactDOM.render(<MiPrueba />, document.getElementById('mi-prueba'));
}
